# face-recognize-platform

🐧773323518

## 介绍
基于mtcnn+facenet深度学习模型的Flask人脸特征提取、特征比对平台

## 使用说明

1. 下载模型  链接: https://pan.baidu.com/s/1S0sf2QVQwBArP7plNbZImw 提取码: s024 
2. 放置模型到faceRecognition/model_data目录下
3. 导入conda环境   `conda env create -f conda-env/env.yaml` 并激活
4. 执行 `python app.py` 启动接口服务

## 接口说明

提供docsify接口文档

```shell
docsify serve docs
```

同时也内部集成swagger文档

## 技术栈

- keras
- flask
- cv2

## mtcnn网络

检测人脸

### 原图片

![输入图片说明](https://images.gitee.com/uploads/images/2021/0817/193645_7ee6267c_6512114.jpeg "timg.jpg")

### 检测后

![输入图片说明](https://images.gitee.com/uploads/images/2021/0817/193653_ca657c78_6512114.jpeg "out.jpg")

## facenet网络

使用googlenet InceptionV1训练，可以提取人脸128维特性向量(为了节省空间,所有特征向量均经过zlib压缩和base64转码)

先使用mtcnn获取到人脸，然后进行识别

### 人物-张飞

#### 图片1

![输入图片说明](https://images.gitee.com/uploads/images/2021/0817/193700_a1ac21f7_6512114.jpeg "张飞1.jpeg")

#### 图片2

![输入图片说明](https://images.gitee.com/uploads/images/2021/0817/193708_3ad1503b_6512114.jpeg "张飞2.jpeg")

### 人物-曹操

#### 图片1

![输入图片说明](https://images.gitee.com/uploads/images/2021/0817/193715_cdec3de9_6512114.jpeg "曹操1.jpeg")

#### 图片2

![输入图片说明](https://images.gitee.com/uploads/images/2021/0817/193724_c4f71140_6512114.jpeg "曹操2.jpeg")

### 特征比对(欧式距离)

越是相似的，距离越近

#### 张飞1-张飞2

distance:0.5082446061984381

#### 张飞1-曹操1

distance:0.8603647692832014

#### 张飞1-曹操2

distance:0.8957766429361684

#### 曹操1-曹操2

distance:0.7081217647804329