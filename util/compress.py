import zlib
import base64

def en(l):
    return base64.b64encode(zlib.compress(','.join(list(map(str, l))).encode())).decode()


def de(base64str):
    return list(map(float, zlib.decompress(base64.b64decode(base64str.encode())).decode().split(',')))
