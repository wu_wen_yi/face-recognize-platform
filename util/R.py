error = {'code': 500,
         'msg': {'English': 'Background error', '中文': '后台错误'}
         }

nofound = {'code': 404, 'msg':{'English': 'No found','中文': '找不到入口'}}

methodNotAllowed = {'code': 405, 'msg':{'English': 'The method is not allowed for the requested URL.','中文': '方法不可用'}}

def ok(msg):
    return {'code': 200, 'msg': msg}


def ifm(code, msg):
    return {'code': code, 'msg': msg}
