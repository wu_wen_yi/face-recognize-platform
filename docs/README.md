# 接口说明

## rectangles

检测人脸(mtcnn网络)

### request

| 接口             | 请求方式 | 数据格式  | 参数                       |
| ---------------- | -------- | --------- | -------------------------- |
| /rectangles.form | POST     | Form-data | faceImg:所要识别的图片     |
| /rectangles.json | POST     | Json      | `{"faceImg":"base64编码"}` |

### response

| 参数           | 说明                   |
| -------------- | ---------------------- |
| personNum      | 图片中人脸的数量       |
| topLeftCorner  | 某一个人脸的左上角位置 |
| topRightCorner | 某一个人脸的右下角位置 |
| width          | 某一个人脸矩形框宽度   |
| height         | 某一个人脸矩形框高度   |
| facialFeatures | 某一个人脸五个部位坐标 |

#### 返回样例

```json
{
    "code": 200,
    "msg": {
        "personNum": 1,
        "rectangles": [
            {
                "topLeftCorner": [
                    116.0036734496164,
                    121.91869298732487
                ],
                "topRightCorner": [
                    293.0401359713907,
                    369.6807129036166
                ],
                "width": 177.03646252177433,
                "height": 247.76201991629173,
                "facialFeatures": [
                    [
                        0.9952834248542786,
                        164.9077606201172
                    ],
                    [
                        222.5684356689453,
                        245.78994750976562
                    ],
                    [
                        221.51312255859375,
                        208.3910675048828
                    ],
                    [
                        276.5383605957031,
                        179.0194091796875
                    ],
                    [
                        319.1883850097656,
                        237.62083435058594
                    ]
                ]
            }
        ]
    }
}
```

## recognize

识别人脸特征(facnet网络  GoogLeNet Incepetion V1)

### request

| 接口            | 请求方式 | 数据格式  | 参数                       |
| --------------- | -------- | --------- | -------------------------- |
| /recognize.form | POST     | Form-data | faceImg:所要识别的图片     |
| /recognize.json | POST     | Json      | `{"faceImg":"base64编码"}` |

### response

| 参数     | 说明                                              |
| -------- | ------------------------------------------------- |
| features | 图片中人脸的128维特征数据(经过压缩后的base64编码) |

#### 返回样例

```json
{
    "code": 200,
    "msg": {
      "features": "eJw1llkWFCEMRTekHjIn+9+YN1D9oUdpCMmbqPNPpHq0xk3keOafv+ff/kvLbVT4ZU7t4pnD70faUo+Hzl3MU8ckzmgflvsuahnHdeKYHnW/ixI57dzTk8F6vtVTezL69NixeVc1h2VcXSpVUt7WoYVO71OW3BCvgdJksTQsxOzdRb0uj+Ki7iP6Z4/TSnPMrE5UvPaV36uce8xPRdyaKnTizvAngkZtj1vJaKTfqj6xayfTtgKTuXhKsig2xiE1oAqTuYiIaipocHFJa9WeTmvVqQKgjp383gJA54JM6wD74KAxjnJ5u9PT7nTI4VYASZ+eu1FK1ARE+FMFrLuxuC+HAanH74+hkBP0K6HaCZdbkiUT2KStMqj019AJP2Iwv4juhGe4tbqOzjQ1LjxhRiXfDrUl7lk3WEwkstLJ2/WESACkmgdHdxtikxH1tigamUcM87rtxYO8XJ4qh1IGNqXqc1sJZjMaXBwR79OUgl4JxZQVh417GsAQK7IMLwC73Vj2VDAbECDZ7QaR0TaDSA7zLYSABpeIoqIVLl49QSACHTBg6O+OFx7oJld+6P6OhxYxC6u+vCjEPEoZl3VonDqpb9HgXKYURy7Z5zOOMQNohHg12N6i4JyD1ApEUvTdjhWQ44MNm33aYQO6b0Hm1ndjxd67I59GmvawwJYLwtrpnLvPDv6bgavxtGejZhGdZRIR7P7YQi+cMwob6jiXm6w5oMjl1Jy6YETDFaqXzRLM8HnYC8kgRto2/9yuHkzcqbuK+n4ZUM0UJERn+QemwMIehAoman1FKQlgLO8vchuVPUZPZoOa6fgFA7ZGY5ge63X0g8jOLCa9ysz6YAvDbHQ18ohABErzij8T2O/gkBygtfaSiHtyi0NqAhC/PccwSnILhsMUqCF/ZAP6sooI9RkTxOQs1Vm9iRNXFyCLqyNXCuWPHBrkZiGBBFX2C3P8QEu+MkAV/pJLp4njbMKL6Cy7bSKJszI9GMe4/cW+ob5A4psA/P2SgrCGPd/EzhfwIGI0jj5WKt84TAt9TNyzLP/UAjCDdWMDRF7ooirfWdABeiH+XgE8snV1MHKrXItKnyGteSHSqXW1Bk+kJoNn09Ptx5bU3ICPc+qlOMIhgXSlgVp/qYmBmXCfwCDlH2rrOZQLdWRciby8P+TKps0i9oUDcN2Q4r8kQvcXDiteVI4H9pG5sxSn8Sq4weFP+90cPA3yBC+Z+L2KiKKi9l1AsnZ7TxLfkOjGMKHx2EEZqwMSBrr9GY+XllYNi5JPfd5TR8zCjfZGm/sP3iDDpmger1fKl03w70zFi+UERL2ahplyfYEWEP0dHXvQ3JY7GxsXI2LWETVGR8j2+W6/B1br7FoWX+/MSCxuToDpfB8EnAQxll8m9HPjkrCsIzmevefaNQRRz0aItPfMUpMNeHTF2c8T5N5+zfg1adzDBpC5XuLl2cMvPsGa74oNg8FG9k2z3wVsyRVyfI/Y2ocnDOLsq4jiN44wbmG4+V4iXBI3RjbR68oF25GGgtQF9d7piFLHeAg1a2954KDUWSM0ksPRz3frjS121t/yUUiMkEPd+7S9JxquaZ2DgAMc8XaeRcGXbzyKKPN7s0gP3kB8AY3c+sKFGOJTB8pXTJ+ELPhv8HjKzulPwLWP8pDouSyG/gexqQ+c"
    }
}
```

## compare

计算特征向量之间的欧式距离

### request

| 接口             | 请求方式 | 数据格式 | 参数                                                         | 说明      |
| ---------------- | -------- | -------- | ------------------------------------------------------------ | --------- |
| /compare.json    | POST     | Json     | `{"features1":"128维特征向量的base64编码","features1":"128维特征向量的base64编码"}` | 1对1比对  |
| /compareAll.json | POST     | Json     | `{"featuresList":["128维特征向量的base64编码列表"],"features":"128维特征向量的base64编码"}` | 多对1比对 |

### response

| 参数         | 说明         |
| ------------ | ------------ |
| distance     | 欧式距离     |
| distanceList | 欧式距离列表 |

#### 返回样例

```json
{
    "code": 200,
    "msg": {
        "distance": 0.113940026384824,
        "ifm": "The smaller the distance, the higher the similarity"
    }
}
```

```json
{
    "code": 200,
    "msg": {
        "distanceList": [
            0.113940026384824,
            0.0
        ],
        "ifm": "The smaller the distance, the higher the similarity"
    }
}
```

